# Digital Garden

![digital garden AI generated image](./images/digital_garden.jpg)
## Description

Ce repository est une bouteille à la mer pour une approche plus dynamique de la documentation technique du projet CarHab. C'est un recueil de mes notes Obsidian, d'organigrammes et de tutoriels destinés à faciliter la compréhension et l'utilisation du travail effectué pour CarHab. 
La majorité de ces notes sont indépendantes du projet CarHab et peuvent servir de base de connaissances partagée, qui vise à remplacer un manuel d'utilisation / guide technique PDF de 80 pages denses. Le focus principal est le Machine Learning / Deep Learning et les concepts associés.

Entre autres, l'idée originelle du "Digital Garden" est d'entretenir un jardin d'idées, plus ou moins construites ou complètes, dans lesquelles on peut naviguer et découvrir de manière autonome et non guidée. Tout comme on peut naviguer d'un concept à un autre sur Wikipédia, les notes sont reliées entre elles pour naviguer. 
La visualisation du graph permet également de cartographier l'interconnexion des notes et des concepts entre eux.

## Usage

On peut naviguer dans ce graph de connaissances à travers la page web hébergée sur GitLab en cliquant sur la droite : GitPages ou sur ce lien : [digital-garden](https://digital-garden-betheve-ff4539b5328d87e722420cea05c7e2905bd94833.gitpages.huma-num.fr/)
On peut aussi cloner le repository et utiliser le dossier `public/online-vault` comme Vault Obsidian à rajouter à ses propres notes.
Je recommande l'utilisation d'[Obsidian](https://obsidian.md/) comme outil d'édition Markdown, mais il existe beaucoup d'autres alternatives. 

L'avantage d'avoir une base de connaissances en Markdown est qu'elle est lisible partout pour toujours, avec n'importe quel logiciel lisant le markdown (voir même en texte brut avec Notepad), et permet d'être converti très facilement en [site web statique ](https://en.wikipedia.org/wiki/Static_web_page) à l'aide de plugins Obsidian, ou de générateurs de sites web statique tels que [Hugo](https://gohugo.io/) ou [Quartz](https://quartz.jzhao.xyz/). Ici également, il existe de nombreux générateurs, avec différentes fonctionnalités.

Cela permet de déployer un site web complet en quelques minutes avec une base de connaissances markdown, évitant l'étape d'édition d'HTML, CSS et JS, ce qui est un gain de temps considérable si l'on veut un site web simple, rapide et léger pour de la documentation.

## Support

Pour toute information, vous pouvez m'envoyer un mail à : braz.ma.etheve@univ-st-etienne.fr

## Roadmap

Pour l'instant, une sous partie de ma bibliothèque complète est dans ce repository et est amené à être étoffé. Il y a ici des documents quasi-complets orientés tutoriels / fondamentaux du ML.

## Contributiions
Toute contribution est bonne à prendre, pour cela, cloner le repository sur votre machine, ajouter les fichiers qui vous intéréssent et soumettez un merge request pour être ajouté à la branche principale du git.

## Authors and acknowledgment
Author : Braz-ma Etheve - EVS Isthme - CarHab

acknowledgment : Un grand merci à Cyrille Conord @cconord pour m'avoir introduit au concept de wiki personnel et de l'outil Obsidian ainsi que le soutien technique pour déployer ces idées.

## License
Open-source, clonez autant que vous voulez ce repository à votre guise.